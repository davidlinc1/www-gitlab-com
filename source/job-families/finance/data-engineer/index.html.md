---
layout: job_family_page
title: "Data Engineer"
---

## Responsibilities

* Ensure the Company’s cloud and on-premise data is centralized into a single data warehouse that can support data analysis requirements from all functional groups of the Company. See our bizops effort, [Meltano](https://gitlab.com/meltano/meltano/).
* Create a common data framework so that all company data can be analyzed in a unified manner.
* Build data pipelines from internal databases and SaaS applications (SFDC, Zuora, Marketo, Zendesk, etc.) to populate and feed our data warehouse with high quality, consistent data.
* Drive the creation of architecture diagrams and systems documentation that can be understood and used by business users and other GitLabbers.
* Help to develop and execute a roadmap for system expansion, evaluate existing systems and ensure future systems are aligned with the Company’s data architecture plan.
* Implement a set of processes that ensure any changes in transactional system architecture are documented and their impact on the company’s overall data integrity are considered prior to changes being made.
* Collaborate with all functions of the company to ensure data needs are addressed.
* This position reports to the Manager, Data & Analytics.


## Data Engineer Requirements

* 2+ years hands-on experience in a data analytics or data warehousing role
* Demonstrably deep understanding of SQL and relational databases (Snowflake preferred)
* Experience with a high growth company using on-premise tools and on-demand (SaaS) transactional systems
* Hands-on experience with Python, SQL, and ETL tools like Talend/Kettle, Boomi, Informatica Cloud, etc. Experience with Snowflake is a plus
* Hands-on experience with data pipeline tools within cloud-based environments (Airflow, Luigi, Azkaban, DBT)
* Be Passionate about data, analytics, and automation.
* Experience with open source data warehouse tools
* Strong data modeling skills and familiarity with the Kimball methodology.
* Experience with Salesforce, Zuora, Zendesk and Marketo as data sources and consuming data from SaaS application APIs.
* Share and work in accordance with our values
* Constantly improve product quality, security, and performance
* Write good code, performant code (Python preferred)
* Knowledge of and experience with data-related Python packages
* Desire to continually keep up with advancements in data engineering practices
* Catch bugs and style issues in code reviews
* Ship small features independently

- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).


## Senior Data Engineer Requirements

* All requirements of an Intermediate Data Engineer
* Understand and implement data engineering best practices
* Write _great_ code
* Create smaller merge requests and issues by collaborating with stakeholders to reduce scope and focus on iteration
* Teach and enforce architectural patterns in code reviews
* Ship medium to large features independently
* Generate architecture recommendations and the ability to implement them
* Great communication: Regularly achieve consensus amongst teams
* Perform technical interviews


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Manager, Data & Analytics
* Next, candidates will be invited to schedule a second interview with a Data Engineer on the Data & Analytics team
* Next, candidates will be invited to schedule a third interview with members of the Meltano team
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
