---
layout: markdown_page
title: "Junior Engineers"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What Makes a Good Junior Candidate

A good candidate for a Junior position is someone who is likely to
quickly become an exceptional engineer at GitLab once they learn the requisite
skills. Specifically, this means that a good Junior candidate:

- Is a good fit for our company values
- Demonstrates strong talent, even if their skills/experience are
  limited (that is, they show aptitude for problem solving, systems thinking,
  etc.)
- Has a strong learning mindset
- Is missing requisite skills/experience for an Intermediate position that can
  be learned in 6-24 months of on-the-job experience

Juniors are an investment - they will require more support with time
and energy during the early stage of their career at GitLab, so they 'cost' more
productivity than a higher-level candidate. Because of this 'cost' we should be
very discerning about who we consider for a Junior position.

**At the moment we are focused on rapidly scaling our team and are very unlikely
to consider accepting any new Juniors.**

## Staffing Juniors

While we reserve the right to make exceptions in atypical situations, Juniors
should be hired with the following staffing restrictions in mind:

- We should not have more than one Junior on any given team.
- We should not have a Junior on any team smaller than 4.
- We should not have a Junior on any team that does not have at least a Senior
  to help mentor and support the Junior in their work.

A team must deliberately open a junior vacany with approval. Junior leveling
should not be done "on the fly" while interviewing intermediate candidates. This
is because it could be used as an excuse to lower the bar for an intermediate
candidate that the interviewer is biased toward, or it could be unfairly applied
to a candidate an interviewer is biased against.

## Supporting Juniors

It's important to provide Juniors with the right level of support so
that they can improve their skillset efficiently. In order to accomplish this,
we should encourage Juniors to tackle any work they think themselves capable of
accomplishing, and be available to help them along the way. Assigning "safe"
work to Juniors may minimize their risk, but it will also stunt their growth. To
use an analogy, we want to make sure we are a good safety net, and not act as
guard rails.

## Coaching Plan

Every Junior should collaborate with their Manager to produce a
coaching plan. This plan should clearly identify skills that the Junior needs to
acquire in order to qualify for an Intermediate level position, and describe how
the Manager is going to work with them to help them acquire those skills
efficiently. This help could come in several forms, including:

- Suggested reading or research
- Recommended online classes
- Opportunities to develop those skills in scheduled work
- Pairing sessions with senior members of the team
- etc.

The coaching plan should be reviewed and updated on a regular cadence to make
sure the Junior and their Manager share an understanding about how the Junior is
working to progress in their career.

## Promotion

Promotion for Juniors should happen along the same lines as [promotion
for anyone else](/handbook/engineering/career-development#promotion).
While the coaching plan should help facilitate this promotion, it should not
be viewed as a "promotion checklist" - that is, there is no guarantee that
completion of a coaching plan will result in a promotion, nor is full completion
of a coaching plan a prerequisite for promotion (although it should obviously be
a factor in making that decision).
