---
layout: job_family_page
title: "Marketing Coordinator EMEA"
---

## Marketing Coordinator EMEA

GitLab is looking for a highly motivated marketer to support our EMEA Sales and Marketing Teams. A successful Marketing Coordinator is self-motivated and highly organized, supporting our sales and marketing objectives and has a good understanding of software, tools and proactive communication.

### Responsibilities

- Support and coordinate events and marketing programs to the needs of the EMEA Marketing and Sales Teams.
- Support and coordinate regional marketing execution.
- Swag management for regional events and in support of the sales department.
- Event logistics in support of the team. From helping to book hotels, SWAG management to making sure every organisational aspect of our events are well organized.
- Support in lead and campaign management and marketing reports (Salesforce.com).

### Requirements

- Past experience supporting both sales and marketing objectives and teams.
- 3 years minimum experience in sales or marketing operation in a software/tech company.
- Strong project management and communication skills. This role will require effective collaboration and coordination across internal and external stakeholders.
- Extremely detail-oriented and organized, and able to meet deadlines.
- You share our values, and work in accordance with those values.
- Past experience using Salesforce, Marketo and other sales and marketing tools.

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a screening call with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the EMEA Field Marketing Manager
- Then, candidates will be invited to schedule a second interview with our EMEA SDR Team Lead
- Candidates will then be invited to schedule a third interview with our Regional Sales Director EMEA
- Then, candidates will be invited to schedule a fourth interview with our Senior Director of Marketing and Sales
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).